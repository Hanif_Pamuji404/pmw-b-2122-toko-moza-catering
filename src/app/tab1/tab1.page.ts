import { Component } from '@angular/core';
import { DetailPage } from '../detail/detail.page';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  navCtrl: any;

  constructor() {}
  onDetailPage(makanan)
  {
    this.navCtrl.push(DetailPage,makanan);
  }
}
